package com.company;
import java.io.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public  class Main {
    public static char[] firstTree = new char[101];

    public static boolean GetInfo(String path) {
        char[] numbers = new char[101];
        try (FileInputStream fin = new FileInputStream(path)) {
            int i = -1, k = 0;
            while ((i = fin.read()) != -1) {
                if (i != ' ') {
                    firstTree[k] = (char)i;
                    k++;
                }
            }
            return  true;
        } catch (IOException ex) {
            System.out.print(ex.getMessage());
            return false;
        }
    }

    public static void main(String arg[]) throws IOException {
        System.out.println("Введите имя файла: ");
        BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
        String path;
        path = bReader.readLine();
        if(GetInfo(path)) {
            BinarySearchTree b = new BinarySearchTree();
            for (int i = 0; firstTree[i] != 0; i++) {
                b.InsertNode(firstTree[i]);
            }
            System.out.print("Введите искомые элемент: ");
            char searchEl = (char) bReader.read();
            System.out.println("Первоначальное дерево : ");
            b.DisplayTree(b.root, 1);
            if (b.DeleteNode(searchEl)) {
                System.out.println("\nЭлемент " + searchEl + " удален : ");
                b.DisplayTree(b.root, 1);
            } else System.out.println("\nЭлемент " + searchEl + " не найден ");
        }
    }
}
