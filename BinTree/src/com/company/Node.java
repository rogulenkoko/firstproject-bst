package com.company;
class Node{
    char info;
    Node left;
    Node right;
    public Node(char info){
        this.info = info;
        left = null;
        right = null;
    }
}