package com.company;
public class BinarySearchTree {
    public static  Node root;
    public BinarySearchTree(){
        this.root = null;
    }

    public boolean DeleteNode(char elem){
        Node parent = root;
        Node current = root;
        boolean isLeftChild = false;
        while(current.info!=elem){
            parent = current;
            if(current.info>elem){
                isLeftChild = true;
                current = current.left;
            }else{
                isLeftChild = false;
                current = current.right;
            }
            if(current ==null){
                return false;
            }
        }
        if(current.left==null && current.right==null){
            if(current==root){
                root = null;
            }
            if(isLeftChild ==true){
                parent.left = null;
            }else{
                parent.right = null;
            }
        }
        else if(current.right==null){
            if(current==root){
                root = current.left;
            }else if(isLeftChild){
                parent.left = current.left;
            }else{
                parent.right = current.left;
            }
        }
        else if(current.left==null){
            if(current==root){
                root = current.right;
            }else if(isLeftChild){
                parent.left = current.right;
            }else{
                parent.right = current.right;
            }
        }else if(current.left!=null && current.right!=null){
            Node minNode = getMinNode(current);
            if(current==root){
                root = minNode;
            }else if(isLeftChild){
                parent.left = minNode;
            }else{
                parent.right = minNode;
            }
            minNode.left = current.left;
        }
        return true;
    }

    public Node getMinNode(Node curNode) {
        Node minNode = null;
        Node minNodeParent = null;
        Node current = curNode.right;
        while (current != null) {
            minNodeParent = minNode;
            minNode = current;
            current = current.left;
        }
        if (minNode != curNode.right) {
            minNodeParent.left = minNode.right;
            minNode.right = curNode.right;
        }
        return minNode;
    }
    public void InsertNode(char elem) {
        Node newNode = new Node(elem);
        if (root == null) {
            root = newNode;
            return;
        }
        Node current = root;
        Node parent = null;
        while (true) {
            parent = current;
            if (elem < current.info) {
                current = current.left;
                if (current == null) {
                    parent.left = newNode;
                    return;
                }
            } else {
                current = current.right;
                if (current == null) {
                    parent.right = newNode;
                    return;
                }
            }
        }
    }

    public void DisplayTree(Node root,int n) {
        if (root != null) {
            System.out.print(" " + root.info);
            if (root.right != null) {
                DisplayTree(root.right, n + 1);
            } else System.out.println();
            if (root.left != null) {
                for (char i = 1; i <= n; i++) {
                    System.out.print(" ");
                }
                DisplayTree(root.left, n + 1);
            }
        }
    }
}
