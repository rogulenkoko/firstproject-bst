#include "stdafx.h"
#include <stdlib.h>
#include <string>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <cstdlib> 
#include <windows.h>
#include <ctime>
#include <stdio.h>
#include <math.h> 
using namespace std;

ofstream out("output.txt");

struct Node
{
	char info;
	Node *left;
	Node *right;
};

typedef Node *binSTree;

char RootBT(binSTree b)
{
	if (b == NULL) { cerr << "Error: RootBT(null) \n"; exit(1); }
	else return b->info;
}

binSTree Left(binSTree b)
{
	if (b == NULL) { cerr << "Error: Left(null) \n"; exit(1); }
	else return b->left;
}

binSTree Right(binSTree b)
{
	if (b == NULL) { cerr << "Error: Right(null) \n"; exit(1); }
	else return b->right;
}

char& RootBT1(binSTree& b)
{
	if (b == NULL) { cerr << "Error: RootBT(null) \n"; exit(1); }
	else return b->info;
}

binSTree& Left1(binSTree& b)
{
	if (b == NULL) { cerr << "Error: Left(null) \n"; exit(1); }
	else return b->left;
}

binSTree& Right1(binSTree& b)
{
	if (b == NULL) { cerr << "Error: Right(null) \n"; exit(1); }
	else return b->right;
}

bool GetInfoFromFile(char* str)
{
	string filename;
	cout << "������� ��� �����: ";
	cin >> filename;
	ifstream in;
	in.open(filename);
	if (!in.is_open())
	{
		cout << "\n���� '" << filename << "' �� ����� ���� ������!\n"; system("pause"); return false;
	}
	else
	{
		char bull, temp;
		int n = 0, i = 0;
		in >> bull;
		if (in.eof())
		{
			cout << "���� ����!!!" << endl; system("pause");
			return 0;
		}
		else
		{
			in.close();
			in.open(filename);
			while (!in.eof())
			{
				in >> temp;
				n++;
			}
			in.close();
			in.open(filename);
			for (i = 0; i<n - 1; i++)
			{
				in >> temp;
				str[i] = temp;
			}
			str[i] = '\0'; return true;
		}
	}
}

void InsertElements(char smb, binSTree &myTree)
{

	binSTree localTree;
	if (myTree == NULL)
	{
		localTree = new Node;
		if (localTree != NULL) {
			localTree->info = smb;
			localTree->left = NULL;
			localTree->right = NULL;
			myTree = localTree;
		}
		else { cerr << "Memory not enough\n"; exit(1); }
	}
	else if (smb < myTree->info) InsertElements(smb, myTree->left);
	else if (smb > myTree->info) InsertElements(smb, myTree->right);
	//else myTree->count++;
}

void DisplayTree(binSTree localTree, int n)
{
	if (localTree != NULL)
	{
		cout << ' ' << localTree->info; out << ' ' << localTree->info;
		if (localTree->right != NULL) { DisplayTree(localTree->right, n + 1); }
		else { cout << endl; out << endl; }
		if (localTree->left != NULL) {
			for (int i = 1; i <= n; i++) { cout << "  "; out << "  "; }
			DisplayTree(localTree->left, n + 1);
		}
	}
}

binSTree SearchElement(char smb, binSTree localTree, int &count)
{
	Sleep(1);
	count++;
	char r;
	if (localTree == NULL) return NULL;
	else
	{
		r = RootBT(localTree);
		if (smb == r) return localTree;
		else if (smb<r) return SearchElement(smb, Left(localTree), count);
		else return SearchElement(smb, Right(localTree), count);
	}
}

binSTree DeleteElementRight(binSTree& myTree, binSTree& randTree)
{
	binSTree Elem = NULL;
	if (myTree->left != NULL)
	{
		Elem = DeleteElementRight(myTree->left, randTree);
	}
	else
	{
		randTree->info = myTree->info;
		myTree = myTree->right;
		return myTree;
	}
}

binSTree DeleteElementLeft(binSTree& myTree, binSTree& randTree)
{
	binSTree Elem = NULL;
	if (myTree->right != NULL)
	{
		Elem = DeleteElementLeft(myTree->right, randTree);
	}
	else
	{
		randTree->info = myTree->info;
		myTree = myTree->left;
		return myTree;
	}
}

binSTree SearchAndDeleteElement(char smb, binSTree& myTree)
{
	Sleep(1);
	binSTree Elem;
	char c;
	if (myTree == NULL) return NULL;
	else
	{
		c = RootBT1(myTree);
		if (smb == c)
		{
			if (myTree->left == NULL && myTree->right == NULL) myTree = NULL;
			else
			{
				if (myTree->right != NULL)
				{
					Elem = DeleteElementRight(myTree->right, myTree);
				}
				else
				{
					Elem = DeleteElementLeft(myTree->left, myTree);
				}
			}
			return myTree;
		}
		else if (smb<c) Elem = SearchAndDeleteElement(smb, Left1(myTree));
		else Elem = SearchAndDeleteElement(smb, Right1(myTree));
	}
	return Elem;

}

int SearchSimpleTree(binSTree Tree, char smb)
{
	Sleep(1);
	if (Tree == NULL) return 0;

	if (Tree->info == smb) return 1;
	int res = 0;
	if (Tree->left != NULL) res = SearchSimpleTree(Tree->left, smb);
	if (res == 0 && Tree->right != NULL) res = SearchSimpleTree(Tree->right, smb);
	return res;
}



int main()
{
	setlocale(LC_ALL, "Russian");
	int time, countSearch = 0, countDelete = 0;
	binSTree randTree = NULL, Elem;
	char Tree[50], foundedEl;
	int choice = 2;
	if (GetInfoFromFile(Tree))
	{
		while (choice == 1 || choice == 2)
		{
			if (choice == 2)
			{
				randTree = NULL;
				for (int i = 0; Tree[i] != '\0'; i++)
				{
					InsertElements(Tree[i], randTree);
				}
			}

			DisplayTree(randTree, 1);
			cout << "������� ������� ������� " << endl;
			cin >> foundedEl;
			time = clock();
			if (SearchElement(foundedEl, randTree, countSearch) != NULL)
			{
				time = clock() - time;
				cout << "������� " << foundedEl << " � ������ ������ ������ �� " << (((double)time) / CLOCKS_PER_SEC) << "c. �������� ������ ���������� " << countSearch << " ���." << endl;
				out << "������� " << foundedEl << " � ������ ������ ������ �� " << ((double)time) / CLOCKS_PER_SEC << "c. �������� ������ ���������� " << countSearch << " ���." << endl;

				time = clock();
				int f = SearchSimpleTree(randTree, foundedEl);
				time = clock() - time;
				cout << "��� ��������� ���� �� ������� ������ ������ ���������� �� " << (((double)time) / CLOCKS_PER_SEC) << "c." << endl;
				out << "��� ��������� ���� �� ������� ������ ������ ���������� �� " << (((double)time) / CLOCKS_PER_SEC) << "c." << endl;

				SearchAndDeleteElement(foundedEl, randTree);
				cout << "������� " << foundedEl << " ������. ������������ ���" << endl;
				out << "������� " << foundedEl << " ������. ������������ ���" << endl;
				DisplayTree(randTree, 1);
			}
			else
			{
				cout << "������� " << foundedEl << " � ������ ������ �� ������" << endl;
				out << "������� " << foundedEl << " � ������ ������ �� ������" << endl;
			}
			system("pause");
			cout << "������� 1, ���� ������ ������� ������� �� ������������� ���" << endl;
			cout << "������� 2, ���� ������ ������� ������� �� ���������� ���" << endl;
			cout << "������� 3, ���� ������ ����� �� ���������" << endl;
			cin >> choice;
			system("cls");
		}
	}
	else return 0;
	return 0;
}
